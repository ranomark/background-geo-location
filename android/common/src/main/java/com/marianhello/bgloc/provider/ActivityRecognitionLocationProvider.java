package com.marianhello.bgloc.provider;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.marianhello.bgloc.Config;
import com.marianhello.bgloc.data.BackgroundActivity;
import com.google.android.gms.location.FusedLocationProviderClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class ActivityRecognitionLocationProvider extends AbstractLocationProvider implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = "LocationService";
    private static final String P_NAME = " com.marianhello.bgloc";
    private static final String DETECTED_ACTIVITY_UPDATE = P_NAME + ".DETECTED_ACTIVITY_UPDATE";
    static JSONArray jsonArray = new JSONArray();
    private static long lastUpdated;
    private static String fileName = "location.txt";
    private GoogleApiClient googleApiClient;
    private PendingIntent detectedActivitiesPI;

    private boolean isStarted = true;
    private boolean isTracking = false;
    private boolean isWatchingActivity = false;
    private Location lastLocation;
    private DetectedActivity lastActivity = new DetectedActivity(DetectedActivity.UNKNOWN, 100);
//    private static final String BASE_URL = "http://192.168.8.108:3000";
    private static final String BASE_URL = "https://tracker.tictoctrack.com/api/UserMobileApp";
    private static RequestQueue mRequestQueue;
    private static Context context;

    public ActivityRecognitionLocationProvider(Context context) {
        super(context);
        PROVIDER_ID = Config.ACTIVITY_PROVIDER;
        setContext( context );
    }

    private final void setContext( Context context )
    {
        this.context = context;
    }

    private static final Context getContext()
    {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Intent detectedActivitiesIntent = new Intent(DETECTED_ACTIVITY_UPDATE);
        detectedActivitiesPI = PendingIntent.getBroadcast(mContext, 9002, detectedActivitiesIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        registerReceiver(detectedActivitiesReceiver, new IntentFilter(DETECTED_ACTIVITY_UPDATE));

    }

    //Function is called to store the locations in a JSON file when there is no internet the location updates failed to react the server.
    public static void storeLocations(Location location) {
        Log.d("storeLocationsCalled", "storeLocation called");
        long now = System.currentTimeMillis();
        final TimeZone tz = TimeZone.getDefault();

        try {
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("latitude", String.valueOf(location.getLatitude()));
            jsonObject.put("longitude", String.valueOf(location.getLongitude()));
            jsonObject.put("speed", String.valueOf(location.getSpeed()));
            jsonObject.put("isLocated", "true");
            jsonObject.put("isSOS", "false");
            jsonObject.put("deviceTime", getDateTime());
            jsonObject.put("timeZone", String.valueOf(tz.getID()));
            jsonObject.put("batteryLevel", String.valueOf(findBatterLevel()));

            lastUpdated = now;


            jsonArray.put(jsonObject);

            appendLog("Location" + "," + " storeLocations "+ "," +" Storing missed locations with  " + jsonArray);
            Log.d(TAG," store locations called " + jsonArray);

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    context.openFileOutput(fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(jsonArray.toString());
            outputStreamWriter.close();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG , "JsonObject is being stored to array " + jsonArray);

    }


    public static void appendLog(String text)
    {
        File logFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "new-library.txt");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.d(TAG,"Error creating file" + e);
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(System.currentTimeMillis() + " , " + text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.d(TAG,"Error writing file" + e);

        }
    }

    @Override
    public void onStart() {
        logger.info("Start recording");
        this.isStarted = true;
        attachRecorder();
    }

    @Override
    public void onStop() {
        logger.info("Stop recording");
        this.isStarted = false;
        detachRecorder();
        stopTracking();

    }

    @Override
    public void onConfigure(Config config) {
        super.onConfigure(config);
        if (isStarted) {
            onStop();
            onStart();
        }
    }

    ///function is called to send bulk updates to server
    public static void bulkUpdateLocations() {
        File file = new File(context.getFilesDir(), "location.txt");
        final JSONArray bulkUpdateArray = new JSONArray();
        RequestQueue requestQueue = null;
        SharedPreferences sharedPreferences = context.getSharedPreferences("DATA", Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("TOKEN", null);

        //Check if locations file exists, if not don't do anything
        if (file.exists()) {
            String ret = "";
            try {
                InputStream inputStream = context.openFileInput("location.txt");

                //Check if file can be accessible
                if (inputStream != null) {
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String receiveString = "";
                    StringBuilder stringBuilder = new StringBuilder();

                    //When the file can be readable apped that to a string
                    while ((receiveString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(receiveString);
                    }

                    inputStream.close();
                    ret = stringBuilder.toString();

                    JSONArray jarray = new JSONArray(ret);
                    Log.d("jArray", jarray.toString());

                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject row = jarray.getJSONObject(i);
                        Log.d("row", row.toString());

                        String date = row.getString("deviceTime");
                        Log.d("deviceTimes", date);

                        String formattedDate = date.toString().replace("\\", "");
                        Log.d("deviceFormateedtime", formattedDate);

                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        dateFormat.setTimeZone((TimeZone.getTimeZone("UTC")));
                        Date dataDate = dateFormat.parse(formattedDate);
                        Date currentDate = dateFormat.parse(getDateTime());

                        long distance = currentDate.getTime() - dataDate.getTime();
                        int minutes = (int) (distance / (1000 * 60)) % 60;

                        Log.d("distance ", String.valueOf(distance));
                        Log.d("distance is milli", String.valueOf(minutes));

                        //Check if the last location update is no lesser than 30 minutes. If else ignore it
                        if (minutes <= 30) {
                            Log.d("jsonObect", row.toString());
                            bulkUpdateArray.put(row);
                        }
                        Log.d("bulkUpdateArray", bulkUpdateArray.toString());
                    }

                    //If there is no instnce of RequestQueue present, create an instance, else don't
                    if (requestQueue == null) {
                        requestQueue = Volley.newRequestQueue(context);
                    }

                    final JSONObject jsonObject = new JSONObject();
                    Log.d(TAG , "bulk update array " + bulkUpdateArray);
                    jsonObject.put("userlocationparamlist", bulkUpdateArray);
                    Log.d("jsonObject", String.valueOf(jsonObject));

                    JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, BASE_URL + "/BulkReportLocation",
                            jsonObject, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG,"Succesfully updated bulk locations" + response.toString());
                            String dir = context.getFilesDir().getAbsolutePath();
                            jsonArray = new JSONArray();

                            appendLog("Location" + "," + " bulkUpdateLocations "+ "," +" Missed locations are uploaded to the server  " + jsonObject);

                            //Delete the existing file once location updates sent succesfully to server
                            File f0 = new File(dir, "location.txt");
                            if (f0.delete()) {
                                Log.d(TAG,"file deleted succesfully");
                            } else {
                                Log.d(TAG , "file cannot be deleted, error occured");
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("error occured", String.valueOf(error));
                            appendLog("Location" + "," + " bulkUpdateLocations "+ "," +" There is a problem uploading bulk locations to server  " + jsonObject);
                            VolleyLog.d("Error", "Error: " + error.getMessage());
                        }
                    }) {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            headers.put("Authorization", "Token " + token);
                            return headers;
                        }
                    };

                    req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0 , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    Volley.newRequestQueue(context).add(req);
                }
            } catch (FileNotFoundException e) {
                Log.e(TAG, "File not found: " + e.toString());
            } catch (IOException e) {
                Log.e(TAG, "Can not read file: " + e.toString());
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean isStarted() {
        return isStarted;
    }

    @Override
    public void onLocationChanged(Location location) {
        logger.debug("Location change: {}", location.toString());

        appendLog("Location" + "," + " onLocationChanged "+ "," +" On location changed function called with latitude " + location.getLatitude() + " longitude "+ location.getLongitude() + " speed " + location.getSpeed() );

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);


        if (lastActivity.getType() == DetectedActivity.STILL) {
            Log.d("still","still activity found");
            handleStationary(location);
            stopTracking();
            return;
        }

        showDebugToast("acy:" + location.getAccuracy() + "latitude" + location.getLatitude());
        sendDataToServer(location);
        lastLocation = location;
        handleLocation(location);
    }

    public static RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(context);
        }
        return mRequestQueue;
    }

    //Return the current date and time in the UTC format.
    public static String getDateTime(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone((TimeZone.getTimeZone("UTC")));
        Date date = new Date();
        return dateFormat.format(date);
    }

    private static int findBatterLevel() {
        BatteryManager bm = (BatteryManager) context.getSystemService(context.BATTERY_SERVICE);
        int batLevel = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
            Log.d("battery level", String.valueOf(batLevel));
        } else {
            batLevel = 0;
        }
        return batLevel;
    }

    //Function sends data to server
    public static void sendDataToServer(final Location location){
        SharedPreferences sharedPreferences = context.getSharedPreferences("DATA",
                Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("TOKEN", null);

        //Get the device's current timezone
        final TimeZone tz = TimeZone.getDefault();

        //If device can be connected to internet make a network request else write it to a file.
        mRequestQueue = getRequestQueue(getContext());
        JSONObject locationObject = new JSONObject();
        try {
            locationObject.put("latitude", String.valueOf(location.getLatitude()));
            locationObject.put("longitude", String.valueOf(location.getLongitude()));
            locationObject.put("speed", String.valueOf(location.getSpeed()));
            locationObject.put("isLocated", "true");
            locationObject.put("isSOS", "false");
            locationObject.put("timeZone", String.valueOf(tz.getID()));
            locationObject.put("deviceTime", getDateTime());
            locationObject.put("batteryLevel", String.valueOf(findBatterLevel()));

            final JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, BASE_URL + "/ReportLocation",
                    locationObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG , " location succesfully sent " + response);
                    try {
                        if(response.getString("status").equals("true")){
                            Log.d(TAG,"succesfully sent data");
                            appendLog("Location" + "," + " sendDataToServer "+ "," +" Location sent to server succesfully with latitude " + location.getLatitude() + " longitude " + location.getLongitude() + " speed " + location.getSpeed() + " date " + getDateTime() + " time zone " + tz.getID() + " \n ");
                        }else{
                            Log.d(TAG,"data cannot be sent succesfully");
                            appendLog("Location" + "," + " sendDataToServer "+ "," +" Server returned an error " + response.getString("message") + " \n ");
                            storeLocations(location);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    bulkUpdateLocations();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "error occured " + error);
                    appendLog("Location" + "," + " sendDataToServer " + "," + " Location is failed to upload with latitude " + location.getLatitude() + " longitude " + location.getLongitude() + " speed " + location.getSpeed());
                    appendLog("Location" + "," + " sendDataToServer failed with " + error );

                    storeLocations(location);

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("oldtoken", token);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("jsonObject", String.valueOf(jsonObject));

                    JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, BASE_URL + "/RenewToken",
                            jsonObject, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Response got", String.valueOf(response));
                            appendLog("Location" + "," + " token regenerated " + ", " + response );

                            try {
                                JSONObject userAppToken = response.getJSONObject("userapptoken");
                                String token = userAppToken.getString("authToken");
                                Log.d("token", token);

                                SharedPreferences sharedPreferences = context.getSharedPreferences("DATA", Context.MODE_PRIVATE);
                                sharedPreferences.edit().putString("TOKEN", token).apply();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("error occured", String.valueOf(error));
                            appendLog("Location" + "," + " token regenerated error " + ", " + error );
                            VolleyLog.d("Error", "Error: " + error.getMessage());
                        }
                    });

                    Volley.newRequestQueue(context).add(req);
                    error.printStackTrace();
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Authorization", "Token " + token);
                    Log.d(TAG, "token " + headers);
                    return headers;
                }
            };

            req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0 , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            Volley.newRequestQueue(context).add(req);

            Log.d(TAG, " location object " + locationObject);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void startTracking() {
        if (isTracking) { return; }

        Integer priority = translateDesiredAccuracy(mConfig.getDesiredAccuracy());
        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(priority) // this.accuracy
                .setFastestInterval(mConfig.getFastestInterval())
                .setInterval(mConfig.getInterval());
        // .setSmallestDisplacement(mConfig.getStationaryRadius());
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            isTracking = true;
            logger.debug("Start tracking with priority={} fastestInterval={} interval={} activitiesInterval={} stopOnStillActivity={}", priority, mConfig.getFastestInterval(), mConfig.getInterval(), mConfig.getActivitiesInterval(), mConfig.getStopOnStillActivity());
        } catch (SecurityException e) {
            logger.error("Security exception: {}", e.getMessage());
            this.handleSecurityException(e);
        }
    }

    public void stopTracking() {
        Log.d(TAG,"stop tracking");
        if (!isTracking) { return; }

        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        isTracking = false;
    }

    private void connectToPlayAPI() {
        logger.debug("Connecting to Google Play Services");
        googleApiClient =  new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                //.addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }

    private void disconnectFromPlayAPI() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    private void attachRecorder() {
        logger.debug("attaching recorder");

        if (googleApiClient == null) {
            connectToPlayAPI();
        } else if (googleApiClient.isConnected()) {
            if (isWatchingActivity) { return; }
            startTracking();
            if (mConfig.getStopOnStillActivity()) {
                ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(
                        googleApiClient,
                        mConfig.getActivitiesInterval(),
                        detectedActivitiesPI
                );
                isWatchingActivity = true;
            }
        } else {
            googleApiClient.connect();
        }
    }

    private void detachRecorder() {
        if (isWatchingActivity) {
            logger.debug("Detaching recorder");
            ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(googleApiClient, detectedActivitiesPI);
            isWatchingActivity = false;
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        logger.debug("Connected to Google Play Services");
        if (this.isStarted) {
            attachRecorder();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // googleApiClient.connect();
        logger.info("Connection to Google Play Services suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        logger.error("Connection to Google Play Services failed");
    }

    /**
     * Translates a number representing desired accuracy of Geolocation system from set [0, 10, 100, 1000].
     * 0:  most aggressive, most accurate, worst battery drain
     * 1000:  least aggressive, least accurate, best for battery.
     */
    private Integer translateDesiredAccuracy(Integer accuracy) {
        if (accuracy >= 10000) {
            return LocationRequest.PRIORITY_NO_POWER;
        }
        if (accuracy >= 1000) {
            return LocationRequest.PRIORITY_LOW_POWER;
        }
        if (accuracy >= 100) {
            return LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
        }
        if (accuracy >= 10) {
            return LocationRequest.PRIORITY_HIGH_ACCURACY;
        }
        if (accuracy >= 0) {
            return LocationRequest.PRIORITY_HIGH_ACCURACY;
        }

        return LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
    }



    public static DetectedActivity getProbableActivity(ArrayList<DetectedActivity> detectedActivities) {
        int highestConfidence = 0;
        DetectedActivity mostLikelyActivity = new DetectedActivity(0, DetectedActivity.UNKNOWN);
        Log.d("DetecedActivity","Detected from here");

        for(DetectedActivity da: detectedActivities) {
            if(da.getType() != DetectedActivity.TILTING || da.getType() != DetectedActivity.UNKNOWN) {
                Log.w(TAG, "Received a Detected Activity that was not tilting / unknown");
                if (highestConfidence < da.getConfidence()) {
                    highestConfidence = da.getConfidence();
                    mostLikelyActivity = da;
                }
            }
        }
        return mostLikelyActivity;
    }

    private BroadcastReceiver detectedActivitiesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            ArrayList<DetectedActivity> detectedActivities = (ArrayList) result.getProbableActivities();

            //Find the activity with the highest percentage
            lastActivity = getProbableActivity(detectedActivities);
            Log.d("Detected","Detected activity");
            logger.debug("Detected activity={} confidence={}", BackgroundActivity.getActivityString(lastActivity.getType()), lastActivity.getConfidence());

            handleActivity(lastActivity);

            if (lastActivity.getType() == DetectedActivity.STILL) {
                showDebugToast("Detected STILL Activity");
                // stopTracking();
                // we will delay stop tracking after position is found
            } else {
                showDebugToast("Detected ACTIVE Activity");
                startTracking();
            }
            //else do nothing
        }
    };

    @Override
    public void onDestroy() {
        logger.info("Destroying ActivityRecognitionLocationProvider");
        onStop();
        disconnectFromPlayAPI();
        unregisterReceiver(detectedActivitiesReceiver);
        super.onDestroy();
    }
}

