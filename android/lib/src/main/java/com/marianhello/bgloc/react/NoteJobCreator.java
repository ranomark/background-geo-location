package com.marianhello.bgloc.react;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

class NoteJobCreator implements JobCreator {
    @Nullable
    @Override
    public Job create(@NonNull String tag) {
        switch (tag) {
            case NoteSyncJob.TAG:
                return new NoteSyncJob();
            default:
                return null;
        }
    }
}
