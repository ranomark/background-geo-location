package com.marianhello.bgloc.react;

import android.support.annotation.NonNull;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.marianhello.bgloc.BackgroundGeolocationFacade;

import java.util.Set;
import java.util.concurrent.TimeUnit;


public class NoteSyncJob extends Job {
    public static final String TAG = "job_note_sync";
    private BackgroundGeolocationFacade facade;

    @Override
    @NonNull
    protected Result onRunJob(@NonNull Params params) {
        facade.start();
        return Result.SUCCESS;
    }

    public static void scheduleJob() {
        Set<JobRequest> jobRequests = JobManager.instance().getAllJobRequestsForTag(NoteSyncJob.TAG);
        if (!jobRequests.isEmpty()) {
            return;
        }
        new JobRequest.Builder(NoteSyncJob.TAG)
                .setPeriodic(TimeUnit.HOURS.toMillis(22), TimeUnit.HOURS.toMillis(20))
                .setUpdateCurrent(true) // calls cancelAllForTag(NoteSyncJob.TAG) for you
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .build()
                .schedule();
    }
}
